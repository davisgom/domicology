<?php
  ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean();
?>

<?php if (isset($theme_header)){
  echo '
    <header class="'.$theme_header.'-header theme-header">
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-8 py-5">
            <h1>'.$page_title.'</h1>

            '.$theme_header_content.'
          </div>
        </div>
    </div>
  </header>
  ';
}
?>

<header class="page-header">
  <div class="container">
    <div class="row py-5">
      <div class="col-12 col-sm-11 d-flex">
        <h1 <?php if (isset($page_title_display)){echo 'class="'.$page_title_display.'"';} ?>>
          <span class="page-title">
            <?php
              if (isset($page_title)){
                echo $page_title;
              }
              
              else echo $page_content; 
            ?>
          </span>
        </h1>
      </div>
    </div>
  </div>
</header>

<section class="container">
  <div class="row">
    <div class="col-11">
      <?php echo $content ?>
    </div>
  </div>
</section>
