<button class="menu-btn btn btn-light d-md-block d-lg-none d-print-none" onclick="navToggle()" onkeydown="navToggle()">
  <span class="menu-icon">
    <i class="fas fa-bars"></i> <br /> <span>Menu</span>
  </span>
  <span class="search-icon">
    <i class="fas fa-search fa-lg"></i>
  </span>
</button>

<section id="siteNav" class="overlay">
  <div class="overlay-content">
    <div class="container">
      <a class="closebtn d-lg-none" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>

      <div id="MSUSearchTool" class="no-fill d-lg-none" role="search">
    			<?php include("msu-search.php"); ?>
    	</div>

      <nav class="row">
        <h1 class="sr-only">Site Navigation</h1>
        <ul class="nav nav-pills nav-fill site-nav" role="navigation">
            <li>
              <a href="home">
                home
              </a>
            </li>

            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                about
              </a>

              <div class="dropdown-menu">
                <span>
                  <a class="dropdown-item <?php if ($page_content == "about") {echo "active";} ?>" href="about">What is Domicology?</a>
                  <a class="dropdown-item <?php if ($page_content == "partners") {echo "active";} ?>" href="partners">partners</a>
                  <a class="dropdown-item <?php if ($page_content == "staff") {echo "active";} ?>" href="staff">staff</a>
                </span>
              </div>
            </li>

            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              research &amp; policy
              </a>

              <div class="dropdown-menu">
                <span>
                  <a class="dropdown-item <?php if ($page_content == "research") {echo "active";} ?>" href="research">research documents</a>
                  <a class="dropdown-item <?php if ($page_content == "externalresearch") {echo "active";} ?>" href="externalresearch">external research</a>
                  <a class="dropdown-item <?php if ($page_content == "presentations") {echo "active";} ?>" href="presentations">presentations</a>
                  <a class="dropdown-item <?php if ($page_content == "tools") {echo "active";} ?>" href="tools">tools, models, and policies</a>
                  <a class="dropdown-item <?php if ($page_content == "ceri") {echo "active";} ?>" href="ceri">Comprehensive Economic Recovery Initiative</a>
                </span>
              </div>
            </li>

            <li>
              <a <?php if ($page_content == "news") {echo 'class="active"';} ?> href="news">
                news
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "events") {echo 'class="active"';} ?> href="events">
                events
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "contact") {echo 'class="active"';} ?> href="contact">
                contact
              </a>
            </li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="container closebtn-bottom d-lg-none">
		<a class="closebtn" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>
	</div>
</section>
