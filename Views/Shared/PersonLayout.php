<?php
  ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean();
?>

<header class="page-header">
  <div class="container">
    <div class="row py-5">
      <div class="col-12 col-sm-11 d-flex">
        <h1 <?php if (isset($page_title_display)){echo 'class="'.$page_title_display.'"';} ?>>
          <span class="page-title">
            Will Spartan
          </span>
        </h1>
      </div>
      <div class="col-12">
        <p class="person-title">
          <strong>Consectetur adipiscing</strong>, Regional Economic Innovation
        </p>
        <p>
          <a href="#">quisque@msu.edu</a>

          <span class="person-links">
            <span class="mx-2 text-muted">|</span>

            <a href="#"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>
  
            <a href="#"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>
  
            <a href="#"><i class="fab fa-instagram-square" aria-hidden="true"></i></a>
  
            <a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
          </span>
        </p>
      </div>

      

    </div>
  </div>
</header>

<section class="container">
  <div class="row">
    <div class="col-12">
      <?php echo $content ?>
    </div>
  </div>
</section>
