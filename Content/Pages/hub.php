<?php
$page_title="Materials Salvage and Reuse Innovation Hub"; 

$page_title_display="d-none";

$theme_header = "hub";

$theme_header_content =
"
    <p>
      The Domicology team at the CCED has been awarded a $600,000 state grant by the Michigan Department of Environment, Great Lakes & Energy (EGLE).
    </p>
";


?>

<p>
  The project has two primary objectives:
</p>

<ol>
  <li>
    Conduct pioneering research on value added reuses of salvaged wood (organic) products present in abandoned structures to bring that material back into the marketplace, and to,
  </li>
  
  <li>
    Create a statewide salvage/reuse business accelerator that will provide strategic training, technical assistance and networking to improve the viability of this nascent industry sector and expand businesses' recycling markets for salvaged materials.
  </li>
</ol>

<p>
  This generous state funding will enable the Domicology team to lead the effort to divert organic material (wood) from the landfill stream and create a viable circular economic sector that can salvage and effectively reuse materials generated from the estimated 226,000 blighted and abandoned residential structures in Michigan
</p>

<section class="container mt-5">
  <div class="row">
    <div class="col-12 col-md network-box">
      <h2>Join our network</h2>
      <p>
        Join our small business email list to receive tips and ideas on marketing, operations, financial and environmental sustainability, and more!
      </p>

      <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed">Sign up!</a>
    </div>

    <div class="col-12 col-md training-box">
      <h2>Training and Technical Assistance</h2>
      <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur pariatur cumque porro voluptates expedita.
      </p>

      <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed">Learn More</a>
    </div>
  </div>
</section>