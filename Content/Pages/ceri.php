<?php
    $page_title="Comprehensive Economic Recovery Initiative";
?>


<p>
    In partnership with the U.S. Department of Commerce, Economic Development Administration (EDA), and to advance Michigan’s long-term economic recovery from COVID-19, the Michigan State University Comprehensive Economic Recovery Initiative (CERI) was established in July 2020. Funded through the CARES Act, CERI will provide education, training, technical assistance and research in partnership with hard hit communities and regions in Michigan from through June 2022.
</p>

<h2 class="mt-5">
    Strategic Areas
</h2>

<p>
    CERI will partner with communities in four areas:
</p>

<ul class="spaced-list">
	<li><strong>Resiliency Planning</strong> - To facilitate comprehensive planning that can assist communities to speed their recovery after experiencing natural or manmade shocks such as the novel coronavirus pandemic.&nbsp;</li>
	
    <li><strong>Financial Resiliency</strong> - Collaborate with community capital experts, state planning agencies, local governments, community anchor institutions, Michigan Economic Development Corporation (MEDC), private financial institutions, and others to increase and expand the capacity of community financial institutions and individuals to invest in local businesses. This initiative will support strategic initiatives to build Michigan's capacity to make community capital investing more available and normalized in Michigan, mitigating the vicissitudes of other less accessible and viable capital markets for small businesses and main street.</li>
	
    <li><strong>Circular Economies</strong> - This strategic initiative will provide technical assistance to accelerate the growth of circular economy businesses in the State of Michigan. This initiative will identify economic recovery efforts and other strategic resources to assist individuals, businesses, and communities to navigate coronavirus-related resources, support feasibility studies, market research, economic impact surveys, and other analysis that support the development of businesses and services that maximize the efficient use of local/regional supply chains in specific markets. Learn more:&nbsp;<a href="/upload/whitepapers/epr-event_wp-web.pdf" target="_blank" class="document pdf">Building a More Sustainable Economy in Michigan: Priority Actions for Supporting an Extended Producer and Circular Economy</a></li>
	
    <li><strong>21st Century Communications Infrastructure</strong> - This initiative will work with key partners to develop and support activities that increase access in rural and low-income urban communities and build the capacity of communities to use these online platforms for civic engagement, commerce, and education.</li>
</ul>

<br />

<p class="alert alert-warning">
    Please stay up to date on REI's monthly E-Updates for upcoming information and on how to get involved with CERI.
</p>