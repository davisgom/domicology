<?php
    $page_title="External Research";
?>

<section class="intro">
    <div class="row">
        <div class="col">
            <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis excepturi eligendi mollitia explicabo quia nesciunt ratione sunt perspiciatis voluptatibus eius ducimus unde porro harum, blanditiis perferendis a repellat voluptatum ipsa. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis excepturi eligendi mollitia explicabo quia nesciunt ratione sunt perspiciatis voluptatibus eius ducimus unde porro harum, blanditiis perferendis a repellat voluptatum ipsa.
            </p>
        </div>
    </div>
</section>

<section>
    <div class="row">
        <div class="col">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="#">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></li>
                <li class="list-group-item"><a href="#">Tempore, quo eos. Tenetur adipisci quidem ut sint porro officia.</a></li>
                <li class="list-group-item"><a href="#">Fugiat deserunt dolore sed modi. Veniam, ipsum dolores?</a></li>
                <li class="list-group-item"><a href="#">Molestiae a quas veniam?</a></li>
                <li class="list-group-item"><a href="#">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></li>
                <li class="list-group-item"><a href="#">Tempore, quo eos. Tenetur adipisci quidem ut sint porro officia.</a></li>
                <li class="list-group-item"><a href="#">Fugiat deserunt dolore sed modi. Veniam, ipsum dolores?</a></li>
                <li class="list-group-item"><a href="#">Molestiae a quas veniam?</a></li>
            </ul>
        </div>
    </div>
</section>



<section class="container mt-5">
    <div class="row">
        <div class="col-12 alert alert-warning p-4">
            <p>
                As structural abandonment is a global problem, MSU CCED is interested in research on the topic from a variety of perspectives and contributors. If you have conducted research that you feel contributes to better understanding this problem, and would like to share it with our network, please submit it using the online form
            </p>

            <a href="#" class="btn btn-theme btn-theme-primary">Submit Research</a>
        </div>
    </div>
</section>