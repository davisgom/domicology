<h2>Advisory Committee List</h2>

<img src="/Content/Images/muskegon-county-logo.png" alt="Muskegon County Logo" class="img-fluid my-5 mx-md-5 float-md-right" />

<p>
The <a href="https://www.co.muskegon.mi.us/" <?php echo $external ?>>Muskegon County</a> Deconstruction Economic Cluster Advisory Committee was formed in 2015. It is made up of experts and stakeholders from the Muskegon area and other areas significantly affected by blight around Michigan. During the course of this study, the Advisory Committee met at least bi-monthly in order to receive updates from the researchers and provide valuable insight and guidance on next steps. MSU CCED and WMSRDC would like to extend our sincerest appreciation for the services provided by the Advisory Committee members, without whom, this report would not be possible.
</p>

<ul>
	<li>Moe Ayoub, Planner, City of Dearborn</li>
	<li>T. Arnold Boezaart, Grand Valley State University, Michigan Alternative and Renewable Energy Center</li>
	<li>Leslie G. Brand III, Chief Executive Officer, Supply Chain Solutions Inc.</li>
	<li>Tim Burgess, Land Bank Coordinator, Muskegon County Land Bank Authority</li>
	<li>Stephen Carlson, Senior Planner, Economic Development, WMSRDC</li>
	<li>Sara Damm, Sustainability Coordinator, Muskegon County Sustainability Office</li>
	<li>Christopher Dean, Fire Chief, Muskegon Heights Fire Department</li>
	<li>Chris Doby, Program Officer, Fred A. and Barbara M. Erb Family</li>
	<li>Matt Flechter, Recycling and Marketing Development Specialist, Michigan Department of Environmental Quality</li>
	<li>Ashley Fleser, Executive Director, Muskegon County Habitat for Humanity</li>
	<li>Brad Garmon, Director of Conservation and Emerging Issues, Michigan Environmental Council</li>
	<li>Ed Garner, President &amp;amp; CEO, Muskegon Area First</li>
	<li>Jeremy Haines, Sales and Marketing Manager, Reclaim Detroit</li>
	<li>John Higgs, Padnos Scrap Management and Recycling</li>
	<li>Kristopher Jolley, Marketing and Sales Manager, MSU Surplus and Recycling Center</li>
	<li>Erin Kelly, City of Detroit</li>
	<li>Jack Kennedy, Commissioner, Muskegon County Road Commission</li>
	<li>Erin Kuhn, Executive Director, WMSRDC</li>
	<li>Cindi Langlois, M.Ed., Workforce Training and Account Manager, Office of Academic Affairs, Muskegon Community College</li>
	<li>Adam Lawver, Administrative Associate I/S, MSU IPF Landscape Services</li>
	<li>Connie Maxim-Sparrow, Grants Coordinator, Muskegon County</li>
	<li>Lynn Mulder, Padnos Scrap Management and Recycling</li>
	<li>Kerrin O'Brien, Michigan Recycling Coalition</li>
	<li>Daniel Pratt, Construction Director, Architectural Salvage Warehouse Detroit</li>
	<li>Eve Pytel, Director of Strategic Priorities, Delta Institute</li>
	<li>Andrea J. Riegler, Architect</li>
	<li>Lisa Sabourin, President/CEO, Employers Association of West Michigan</li>
	<li>Byron Turnquist, Commissioner, City of Muskegon</li>
	<li>Jonathan Wilson, Economic Development Coordinator, Muskegon County</li>
</ul>

<br />
<br />

<h2>EDA Funding</h2>

<img src="/Content/Images/eda-logo.svg" alt="EDA Logo" class="img-fluid my-5 mx-md-5 float-md-right" style="max-width: 320px;" />

<p>
In September 2015, the CCED and <a href="http://wmsrdc.org/" <?php echo $external ?>>West Michigan Shoreline Regional Development Commission</a> were awarded a $223,000 U.S. EDA grant to conduct a study to determine the feasibility of creating an economic sector in Muskegon, MI focused around the recycling and repurposing of materials obtained through the process of deconstructing abandoned structures in the Great Lakes region. The feasibility study stems from years of research on structural abandonment and its consequences, not only on surrounding property values, but also more broadly on the environment and economy. Project Coordinator Adrianna Jordan worked to identify material sources, new uses for the salvaged materials, the logistics of transporting them to the Port of Muskegon, and their economic value. This project is part of an emerging field called "Domicology" defined by CCED Director Dr. Rex LaMore as "the study of abandoned structures, policies, and practices that result in abandonment, and ways that we can mitigate the negative social, environmental and economic implications of that." In this capacity, Domicology will seek to identify a model for future development that will take blight into consideration - designing for deconstruction.
</p>