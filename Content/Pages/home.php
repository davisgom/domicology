<section class="homepage-intro  text-lg-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-8">
        <p>
        The study of policies, practices, and consequences of structural abandonment
        </p>

        <a href="about" class="btn btn-theme btn-theme-accent">
          Learn More
        </a>
      </div>
    </div>
  </div>
</section>


<!-- **** FEATURE AND NEWS SECTION **** -->
<section class="feature-and-news">
  <div class="container">
    <div class="row">
      <div class="feature col-12 col-lg-6 col-xl-8">
        
        <div class="container">

          <!-- **** EXAMPLE FOR FEATURED TEXT ITEM **** -->
          <!-- <div class="row">
            <div class="col-xl-4 mb-3">
              <img src="Content/Images/placeholder-image.jpg" class="img-fluid" />
            </div>

            <div class="col-12 col-xl-8">
              <h2>Title of this featured item</h2>
              <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit sint nobis velit dolore assumenda cumque odit illo! Cum corporis, quidem quasi eum.
              </p>
              <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed ">Learn More</a>
            </div>
          </div> -->

          <!-- **** EXAMPLE FOR VIDEO ITEM **** -->
          <div class="row">
            <div class="col-12">
              <div class="embed-responsive embed-responsive-16by9">
              
                <video id="placeholder-video" class="embed-responsive-item video-js" controls="" preload="metadata" title="Placeholder Video" style="background-color: transparent;" data-setup="{}" poster="/Content/Images/video-placeholder-poster.jpg">
                  <source src="/Content/Videos/city-video-placeholder.mp4" type="video/mp4">
                </video>
              </div>
            </div>
          </div>
        </div>

      </div>
      
      <div class="news col-12 col-lg-6 col-xl-4">
        
        <div class="container">
          <div class="row">
            <div class="col">
              <h2>Research</h2>

              <!-- **** IMPORTANT: FOR DESIGN PLEASE ONLY DISPLAY THREE ITEMS!! **** -->

              <article>
                <h3><a href="#">Transforming the 21st Century Built Environment: Selected Student Papers in Domicology, Volume IV</a></h3>
              </article>

              
              <article>
                <h3><a href="">2 Case Studies in Material Salvage and Reuse</a></h3>
              </article>
              
              <article>
                <h3><a href="">Pathways in Domicology: Deconstruction Workforce Development</a></h3>
              </article>
              <br />

              <a href="#" class="btn btn-sm btn-outline-secondary">
                View all
              </a>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div> 
</section>

<!-- **** FEATURE SECTION **** -->

<section class="msri-feature">
  <div class="container">
    <div class="row justify-content-center">
      <div class ="msri-intro">
        <div class="col-12 col-md-5">
          <h2 class="msri-title text-hide">
            Materials Salvage and Reuse Innovation Hub
          </h2>
        </div>

        <div class="col-12 col-md-8">
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sunt incidunt reprehenderit dolorem vitae quae, quam, natus nam cupiditate suscipit libero eveniet veniam illum quo possimus quod quisquam culpa earum fugiat.
          </p>

          <a href="hub" class="btn btn-theme-outline btn-theme-outline-reversed">
            Learn More
          </a>
        </div>
      </div>  
    </div>
  </div>  
</section>

<!-- **** SOCIAL MEDIA BUTTONS SECTION **** -->

<?php //include("Views/Shared/Partials/social.php"); ?>

<!-- **** LINKS TO OTHER SITES **** -->

<section class="ced-sites border-top py-5">
  <div class="container">
    <div class="row">
      <div class="col-12 d-flex justify-content-center">    
        
        <a href="#" class="text-hide site cced-logo-color">
          <?php echo $cced; ?>
        </a>

        <span class="d-block"></span>

        <a href="https://cei.mdavis.in" class="text-hide site cei-logo-color">
            Circular Economy Institute
          </a>

        <span class="d-block"></span>

        <a href="#" class="text-hide site rei-logo-color">
          MSU EDA University Center for Regional Economic Innovation
        </a>

      </div>
    </div>
  </div>
</section>