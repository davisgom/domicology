<?php
$page_title="What is Domicology?";
?>


<p class ="lead">
Domicology is the study of the economic, social, and environmental characteristics relating to the life cycle of the built environment.
</p>

<br />

<h2>Domicologists</h2>

<ol>
    <li>
        Recognize that manmade structures have a life cycle
    </li>
    
    <li>
        Examine the life cycle continuum of the built environment and plan, design, construct, and deconstruct in order to maximize the reuse of materials and minimize the negative impacts of a structure's end of useful life
    </li>
    
    <li>
        Identify innovative tools, models, policies, practices, and programs that can sustainably address a structural life cycle
    </li>
    
    <li>
        Conduct research on the technical, economic, and policy challenges present in a structure's life cycle and seek to reduce the negative social, economic, and environmental impacts associated with structural abandonment
    </li>
</ol>

<hr class="divider" />

<p>
    The term was coined in 2015 by Dr. Rex LaMore.
</p>

<p>
    To learn more about the study of Domicology, check out our podcast episode covering the four main tenants of Domicology with the help of experts across the reuse and salvage industry. 
</p>

<!-- <audio controls>
    <source src="/Content/Audio/domicology_podcast_20200109.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio> -->

  <iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1LvxEGUBnrcXhVSMFmLGaB?utm_source=generator" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>